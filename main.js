import Vue from 'vue'
import App from './App'

//导入网络请求的包
import {$http} from '@escook/request-miniprogram'
uni.$http=$http


Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()

// 封装请求失败弹窗
uni.$showMsg = function(title = '数据请求失败！', duration = 1500) {
	uni.showToast({
		title,
		duration,
		icon: 'none'
	})
}
